FROM fedora:latest
MAINTAINER Martin Curlej <martin.curlej@gmail.com>
LABEL description="Demo App"
LABEL summary="Demo App"
USER root

ENV DEMO_APP_VER 0.1

# Install Dependecies
RUN dnf -y install \
        python \
        python-flask \
    && dnf clean all

RUN mkdir /src && chmod 777 /src

ENV FLASK_APP app.py

WORKDIR /src

RUN chmod 755 /src && chown 1001:1001 /src

ENV FLASK_APP app.py

USER 1001
# Non-root setup

COPY . /src

EXPOSE 5000

ENTRYPOINT flask run -h 0.0.0.0 -p 5000
